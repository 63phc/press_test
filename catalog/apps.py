from django.apps import AppConfig


class GalleryConfig(AppConfig):
    name = 'catalog'
    verbose_name = 'Каталог'
