from django.utils.timezone import datetime
from django.db import models


class Photo(models.Model):
    """
    Фотография
    """
    title = models.CharField(verbose_name='Название', max_length=200)
    description = models.TextField(verbose_name='Описание для фотографии', blank=True, null=True)
    image = models.ImageField(verbose_name='Фотография', upload_to='photos/%Y/%m', blank=True)
    data_of_filming = models.DateField(verbose_name='Дата съёмки', default=datetime.now, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'
