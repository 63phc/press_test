from django.conf.urls import url

from catalog import views


urlpatterns = [
    url(r'^$', views.ListPhoto.as_view(), name='photo_list'),
    url(r'^new$', views.PhotoCreate.as_view(), name='photo_new'),
    url(r'^edit/(?P<pk>\d+)$', views.PhotoUpdate.as_view(), name='photo_edit'),
    url(r'^delete/(?P<pk>\d+)$', views.PhotoDelete.as_view(), name='photo_delete'),
]
