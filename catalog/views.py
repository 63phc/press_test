import operator
from functools import reduce
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Photo


class ListPhoto(ListView):
    model = Photo
    template_name = 'photo_list.html'
    context_object_name = 'photos'

    def get_queryset(self):
        result = super(ListPhoto, self).get_queryset()

        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(title__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(description__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(data_of_filming__icontains=q) for q in query_list))
            )
        return result


class PhotoCreate(CreateView):
    model = Photo
    success_url = reverse_lazy('catalog:photo_list')
    fields = ['title', 'description', 'data_of_filming', 'image']


class PhotoUpdate(UpdateView):
    model = Photo
    success_url = reverse_lazy('catalog:photo_list')
    fields = ['title', 'description', 'data_of_filming', 'image']


class PhotoDelete(DeleteView):
    model = Photo
    success_url = reverse_lazy('catalog:photo_list')
