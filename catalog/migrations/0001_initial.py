# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-05 08:56
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Название')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Описание для фотографии')),
                ('image', models.ImageField(blank=True, upload_to='photos/%Y/%m', verbose_name='Фотография')),
                ('data_of_filming', models.DateField(blank=True, default=datetime.datetime.now, verbose_name='Дата съёмки')),
            ],
            options={
                'verbose_name': 'Фотография',
                'verbose_name_plural': 'Фотографии',
            },
        ),
    ]
